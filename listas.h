#ifndef LISTAS_H_INCLUDED
#define LISTAS_H_INCLUDED

#include "personas.h"

typedef struct {
    persona cliente;
    struct nodo * siguiente;
    struct nodo * anterior;
} nodo_lista;

// core
nodo_lista * iniciar_lista();
nodo_lista * nuevo_nodo(persona cliente);
nodo_lista * ultimo_nodo(nodo_lista * lista);
nodo_lista * agregar_final(nodo_lista * lista, nodo_lista * nuevo);
nodo_lista * agregar_principio(nodo_lista * lista, nodo_lista * nuevo);

nodo_lista * agregar_ordenado(nodo_lista * lista, nodo_lista * nuevo);
nodo_lista * ordenar_lista(nodo_lista * lista);

nodo_lista * agregar_ordenado_prioridad(nodo_lista * lista, nodo_lista * nuevo);
nodo_lista * ordenar_lista_prioridad(nodo_lista * lista);

int cantidad_clientes(nodo_lista * lista);

#endif // LISTAS_H_INCLUDED
