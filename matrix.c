#include "matrix.h"

/***************************************************************************
*   @brief  Copia una matriz de 3 x 3
*   @param  matrix_1 , matrix_2*
*   @return void
***************************************************************************/
void copy_matrix(float matrix_1[3][3], float matrix_2[3][3])
{
    int a, b;

    for(a = 0; a < 3; ++a)
        for(b = 0; b < 3; ++b)
            matrix_1[a][b] = matrix_2[a][b];
}

/***************************************************************************
*   @brief Carga una matriz con una llave aleatoria (Encriptacion dinamica)
*   @param (output)
*   @return void
***************************************************************************/
void generate_key(float matrix[3][3])
{
    int a, b;

    float random_number;

    srand(time(NULL)); // generate seed

    for(a = 0; a < 3; ++a) {
        for(b = 0; b < 3; ++b) {
            random_number = (float) (rand() % 19) + 1;
            matrix[a][b] = random_number;
        }
    }
}

/***************************************************************************
*   @brief  Genera la matriz de texto encriptado (3 x 5)
*   @param  'crypted_str'       texto pasado a numeros (float)
*   @param  'value' dimension   valida del vector anterior
*   @param  'crypted_matrix'    (output)
*   @return void
***************************************************************************/
void generate_encrypted_matrix(float crypted_str[50], int value, float crypted_matrix[3][5])
{
    int a, b, counter = 0;

    for(a = 0; a < 5; ++a) {
        for(b = 0; b < 3; ++b) {
            if(counter < value) {
                crypted_matrix[b][a] = crypted_str[counter];
            } else {
                crypted_matrix[b][a] = 0;
            }
            ++counter;
        }
    }
}

/***************************************************************************
*   @brief  pasa un string a una matriz de numeros equivalentes y completa
*           con 0's para llegar a la dimension correspondiente (3 x 5)
*   @param  'original_str'      origen
*   @param  'crypted_matrix'    destino (output)
*   @return void
***************************************************************************/
void encrypt_text(char original_str[15], float crypted_matrix[3][5])
{
    float crypted[15];
    int i = 0;

    while(i < 15 && original_str[i] != '\0')
    {
        crypted[i] = toupper(original_str[i]) - 64;
        ++i;
    }

    generate_encrypted_matrix(crypted, i, crypted_matrix);
}

/***************************************************************************
*   @brief  subfuncion para sacar el producto de una matriz
*   @param  'matrix_1'      matriz a
*   @param  'matrix_2'      matriz b
*   @param  'a // b'        subindices
*   @return (float)
***************************************************************************/
float math_prod(float matrix_1[3][3], float matrix_2[3][3], int a, int b)
{
    int i;
    float ans = 0;

    for(i = 0; i < 3; ++i)
    {
        ans += matrix_1[a][i] * matrix_2[i][b];
    }

    return ans;
}

/***************************************************************************
*   @brief  calcula el producto entre dos matrices
*   @param  'matrix_1'      matriz a
*   @param  'matrix_2'      matriz b
*   @param  'result'        (output)
*   @return void
***************************************************************************/
void matrix_product(float matrix_1[3][3], float matrix_2[3][5], float result[3][5])
{
    int a, b;

    for(a = 0; a < 3; ++a) {
        for(b = 0; b < 5; ++b) {
            result[a][b] = math_prod(matrix_1, matrix_2, a, b);
        }
    }
}

/***************************************************************************
*   @brief  calcula el determinante de una matriz de 3 x 3
*   @param  'matrix' matriz original
*   @return (float) 'determinante de la matriz'
***************************************************************************/
float get_determinant(float matrix[3][3])
{
    int a, b, cont = 0;
    float ans = 0;

    float matrix_aux[3][5];

    for(a = 0; a < 3; a++)
    {
        for(b = 0; b < 5; b++)
        {
            if(b < 3)
            {
                matrix_aux[a][b] = matrix[a][b];
            } else {
                if(b == 3)
                {
                    matrix_aux[a][b] = matrix[a][0];
                } else if (b == 4) {
                    matrix_aux[a][b] = matrix[a][1];
                }
            }
        }
    }

    b = 0;

    while(cont < 3)
    {
        a = 0;
        ans += matrix_aux[a][b] * matrix_aux[a + 1][b + 1] * matrix_aux[a + 2][b + 2];
        cont++;
        b++;
    }

    cont = 0;
    b = 4;

    while(cont < 3)
    {
        a = 0;
        ans -= matrix_aux[a][b] * matrix_aux[a + 1][b - 1] * matrix_aux[a + 2][b - 2];
        cont++;
        b--;
    }

    return ans;
}

/***************************************************************************
*   @brief  transforma la matriz en su adjunta (se modifica la original)
*   @param  (output)
*   @return void
***************************************************************************/
void get_attached(float matrix[3][3])
{
    float cofactores[9];
    float matrix_aux[3][3];
    int a, b;

    cofactores[0] = matrix[1][1] * matrix[2][2] - matrix[1][2] * matrix[2][1];
    cofactores[1] = -1 * (matrix[1][0] * matrix[2][2] - matrix[1][2] * matrix[2][0]);
    cofactores[2] = matrix[1][0] * matrix[2][1] - matrix[1][1] * matrix[2][0];
    cofactores[3] = -1 * (matrix[0][1] * matrix[2][2] - matrix[0][2] * matrix[2][1]);
    cofactores[4] = matrix[0][0] * matrix[2][2] - matrix[0][2] * matrix[2][0];
    cofactores[5] = -1 * (matrix[0][0] * matrix[2][1] - matrix[0][1] * matrix[2][0]);
    cofactores[6] = matrix[0][1] * matrix[1][2] - matrix[0][2] * matrix[1][1];
    cofactores[7] = -1 * (matrix[0][0] * matrix[1][2] - matrix[0][2] * matrix[1][0]);
    cofactores[8] = matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];

    matrix[0][0] = cofactores[0];
    matrix[0][1] = cofactores[1];
    matrix[0][2] = cofactores[2];
    matrix[1][0] = cofactores[3];
    matrix[1][1] = cofactores[4];
    matrix[1][2] = cofactores[5];
    matrix[2][0] = cofactores[6];
    matrix[2][1] = cofactores[7];
    matrix[2][2] = cofactores[8];

    for(a = 0; a < 3; a++)
    {
        for(b = 0; b < 3; b++)
        {
            matrix_aux[a][b] = matrix[b][a];
        }
    }

    copy_matrix(matrix, matrix_aux);
}

/***************************************************************************
*   @brief  transforma a una matriz en su inversa (modifica la original)
*   @param  (output)
*   @return void
***************************************************************************/
void get_inverse(float matrix[3][3])
{
    int a, b;
    float matrix_aux[3][3];
    float deter = get_determinant(matrix);
    get_attached(matrix);

    for(a = 0; a < 3; a++)
    {
        for(b = 0; b < 3; b++)
        {
            matrix_aux[a][b] = matrix[a][b] / deter;
        }
    }

    copy_matrix(matrix, matrix_aux);
}

/***************************************************************************
*   @brief  compara dos matrices, devuelve 0 si son iguales.
*   @param  'matrix_1'
*   @param  'matrix_2'
*   @return (int) el resultado de la comparativa
***************************************************************************/
int compare_matrix(float matrix_1[3][5], float matrix_2[3][5])
{
    int ret = 0;
    int a, b;

    for(a = 0; a < 3; a++)
    {
        for(b = 0; b < 5; b++)
        {
            if(matrix_1[a][b] != matrix_2[a][b])
            {
                ret = 1;
            }
        }
    }

    return ret;
}

