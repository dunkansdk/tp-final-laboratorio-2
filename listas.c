#include <stdio.h>
#include <stdlib.h>

#include "listas.h"

/***************************************************************************
*   @brief  Inicializa una lista
*   @param  (null)
*   @return 'nodo_lista'
***************************************************************************/
nodo_lista * iniciar_lista()
{
    return NULL;
}

/***************************************************************************
*   @brief  Asigna datos a un nodo segun los datos de una estructura persona
*   @param  'cliente' estructura persona cargada
*   @return 'nodo_lista'
***************************************************************************/
nodo_lista * nuevo_nodo(persona cliente)
{
    nodo_lista * lista = (nodo_lista*)malloc(sizeof(nodo_lista));

    strcpy(lista->cliente.apellido, cliente.apellido);
    strcpy(lista->cliente.nombres, cliente.nombres);

    lista->cliente.cantArticulos = cliente.cantArticulos;
    lista->cliente.eliminado = cliente.eliminado;
    lista->cliente.id = cliente.id;
    lista->cliente.medioPago = cliente.medioPago;
    lista->cliente.tipoCliente = cliente.tipoCliente;

    lista->cliente.tiempoEspera = 0;
    lista->cliente.tiempoProcesado = 0;

    lista->anterior = NULL;
    lista->siguiente = NULL;

    return lista;
}

/***************************************************************************
*   @brief  Agrega un nuevo nodo al principio de la lista
*   @param  'lista' nodo principal de la lista original
*   @param  'nuevo' nuevo nodo a insertar en la lista
*   @return 'nodo_lista'
***************************************************************************/
nodo_lista * agregar_principio(nodo_lista * lista, nodo_lista * nuevo)
{
    if(lista == NULL)
    {
        lista = nuevo;
    }
    else
    {
        lista->anterior = nuevo;
        nuevo->siguiente = lista;
    }

    return nuevo;
}

/***************************************************************************
*   @brief  Retorna el ultimo nodo de una lista
*   @param  'lista' nodo principal de la lista original
*   @return 'nodo_lista'
***************************************************************************/
nodo_lista * ultimo_nodo(nodo_lista * lista)
{
    if(lista != NULL)
    {
        if(lista->siguiente != NULL)
            ultimo_nodo(lista->siguiente);
    }

    return lista;
}

/***************************************************************************
*   @brief  Agrega un nodo al final de la lista
*   @param  'lista' nodo principal de la lista original
*   @param  'nuevo' nuevo nodo a insertar en la lista
*   @return 'nodo_lista'
***************************************************************************/
nodo_lista * agregar_final(nodo_lista * lista, nodo_lista * nuevo)
{

    if(lista == NULL)
    {
        lista = nuevo;
    } else {
        nodo_lista * ultimo = lista;// = ultimo_nodo(lista);

        while(ultimo->siguiente != NULL)
            ultimo = ultimo->siguiente;

        ultimo->siguiente = nuevo;
        nuevo->anterior = ultimo;
    }

    return lista;
}

/***************************************************************************
*   @brief  Agrega un nodo a la lista de forma ordenada.
*   @param  'lista' nodo principal de la lista original
*   @param  'nuevo' nuevo nodo a insertar en la lista
*   @return 'nodo_lista'
***************************************************************************/
nodo_lista * agregar_ordenado(nodo_lista * lista, nodo_lista * nuevo)
{
    if(lista == NULL)
    {
        lista = nuevo;
    }
    else if(nuevo->cliente.cantArticulos < lista->cliente.cantArticulos)
    {
        lista = agregar_principio(lista, nuevo);
    }
    else
    {
        nodo_lista * antes  = lista;
        nodo_lista * seguidora = lista->siguiente;

        while(seguidora != NULL && seguidora->cliente.cantArticulos < nuevo->cliente.cantArticulos)
        {
            antes = seguidora;
            seguidora = seguidora->siguiente;
        }

        antes->siguiente = nuevo;
        nuevo->anterior = antes;
        nuevo->siguiente = seguidora;
        if(seguidora != NULL) seguidora->anterior = nuevo;
    }

    return lista;
}

/***************************************************************************
*   @brief  Agrega un nodo a la lista de forma ordenada por prioridad.
*   @param  'lista' nodo principal de la lista original
*   @param  'nuevo' nuevo nodo a insertar en la lista
*   @return 'nodo_lista'
***************************************************************************/
nodo_lista * agregar_ordenado_prioridad(nodo_lista * lista, nodo_lista * nuevo)
{
    if(lista == NULL)
    {
        lista = nuevo;
    }
    else if(nuevo->cliente.tipoCliente < lista->cliente.tipoCliente)
    {
        lista = agregar_principio(lista, nuevo);
    }
    else
    {
        nodo_lista * antes  = lista;
        nodo_lista * seguidora = lista->siguiente;

        while(seguidora != NULL && seguidora->cliente.tipoCliente < nuevo->cliente.tipoCliente)
        {
            antes = seguidora;
            seguidora = seguidora->siguiente;
        }

        antes->siguiente = nuevo;
        nuevo->anterior = antes;
        nuevo->siguiente = seguidora;
        if(seguidora != NULL) seguidora->anterior = nuevo;
    }

    return lista;
}

/***************************************************************************
*   @brief  Ordena una lista segun la cantidad de articulos
*   @param  'lista'
*   @return (nodo_lista)
***************************************************************************/
nodo_lista * ordenar_lista(nodo_lista * lista)
{

    nodo_lista * aux_lista = NULL;

    while(lista != NULL)
    {
        aux_lista = agregar_ordenado(aux_lista, nuevo_nodo(lista->cliente));
        lista = lista->siguiente;
    }

    return aux_lista;
}

/***************************************************************************
*   @brief  Ordena una lista segun la prioridad
*   @param  'lista'
*   @return (nodo_lista)
***************************************************************************/
nodo_lista * ordenar_lista_prioridad(nodo_lista * lista)
{

    nodo_lista * aux_lista = NULL;

    while(lista != NULL)
    {
        aux_lista = agregar_ordenado_prioridad(aux_lista, nuevo_nodo(lista->cliente));
        lista = lista->siguiente;
    }

    return aux_lista;
}

/***************************************************************************
*   @brief  Retorna la cantidad de clientes para dividir por la suma de los
*           articulos
*   @param  (cola)
*   @return cantidad de clientes
***************************************************************************/
int cantidad_clientes(nodo_lista * lista)
{
    int clientes = 1;

    if(lista != NULL) {
        if(lista->siguiente != NULL)
        {
            clientes += cantidad_clientes(lista->siguiente);
        }
    }

    return clientes;
}
