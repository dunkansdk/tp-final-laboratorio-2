#ifndef CAJAS_H_INCLUDED
#define CAJAS_H_INCLUDED

#include "cola.h"
#include "personas.h"
#include "arboles.h"

#define CANTIDAD_CAJAS 4

typedef struct {
    int         nro_de_caja;        // Subindice de la caja
    char        nombre_cajero[40];
    medio_pago  tipo_pago;          // Enum definido en personas.h
    int         estado;
    char        algoritmo[30];      // Unicamente el nombre del algoritmo
    nodo_cola   cola;
} control_caja;

typedef enum algorithm_type {
        NULL_ORDER,
        PREORDER,
        INORDER,
        POSORDER
} order_type;

void iniciar_cajas(control_caja cajas[4]);
void mostrar_cajas_abiertas(control_caja cajas[4]);
void cambiar_estado_caja(control_caja * cajas);
void mostrar_caja(control_caja cajas);

void atender_clientes(control_caja cajas[4]);

void agregar_preorder(nodo_arbol * arbol, control_caja cajas[4]);
void agregar_inorder(nodo_arbol * arbol, control_caja cajas[4]);
void agregar_posorder(nodo_arbol * arbol, control_caja cajas[4]);

nodo_cola agregar_por_tiempo_Determinado (control_caja caja);
void  resultado_por_tiempo_determinado (control_caja caja);

#endif // CAJAS_H_INCLUDED
