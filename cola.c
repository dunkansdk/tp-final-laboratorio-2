#include "cola.h"

/***************************************************************************
*   @brief  inicializa los nodos de la cola (cabecera y ultimo)
*   @param  'cola'  cola que se desea inicializar
*   @return (void)
***************************************************************************/
void iniciar_cola(nodo_cola * cola)
{
    cola->cabecera = iniciar_lista();
    cola->ultimo = iniciar_lista();
}

/***************************************************************************
*   @brief  agrega una persona nueva a la cola
*   @param  'cola' hace referencia a la cola donde se quiere agregar el cliente
*   @param  'cliente' cliente a agregar
*   @return (void)
***************************************************************************/
void agregar_cola(nodo_cola * cola, persona cliente)
{
    nodo_lista * nuevo = nuevo_nodo(cliente);

    if(cola->cabecera == NULL)
    {
        cola->cabecera = nuevo;
        cola->ultimo = nuevo;
    } else {
        cola->cabecera = agregar_final(cola->cabecera, nuevo);
        cola->ultimo = nuevo;
    }
}

/***************************************************************************
*   @brief  ******TODO******: extrae un nodo de la cola
*   @param
*   @return
***************************************************************************/
persona extraer_cola(nodo_cola * cola)
{
    persona cliente_aux;

    if(cola->cabecera != NULL)
    {
        nodo_lista * nodo_aux = cola->cabecera;
        nodo_lista * nodo_sig = nodo_aux->siguiente;

        if(cola->cabecera != cola->ultimo)
        {
            nodo_sig->anterior = NULL;
            cola->cabecera = nodo_sig;
        } else {
            cola->cabecera = NULL;
            cola->ultimo = NULL;
        }

        cliente_aux = nodo_aux->cliente;
        free(nodo_aux);
    }

    return cliente_aux;
}

int cola_vacia(nodo_cola cola)
{
    if (cola.cabecera == NULL && cola.ultimo == NULL) return 1;
    return 0;
}

/***************************************************************************
*   @brief  Muestra el contenido de una cola
*   @param  'nodo_cola' cola que se quiere mostrar
*   @return (void)
***************************************************************************/
void mostrar_cola(nodo_cola cola)
{
    nodo_lista * cola_aux;

    cola_aux = cola.cabecera;

    if(cola_aux == NULL)  printf("[ERROR] La cola no tiene clientes.\n");

    while(cola_aux != NULL)
    {
        printf("[ID: %d] ", cola_aux->cliente.id, cola_aux->cliente.cantArticulos);
        puts(cola_aux->cliente.nombres);
        cola_aux = cola_aux->siguiente;
    }
}
