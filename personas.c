#include "personas.h"
#include <stdio.h>
#include <stdlib.h>
#include "archivo.h"

/***************************************************************************
*   @brief  retorna una estructura persona cargada con datos ingresados
*           por el usuario (alta de una persona)
*   @param  (null)
*   @return persona
***************************************************************************/
persona crear_persona()
{
    persona cliente;
    int lastid = get_last_id(PERSONAS_PATH, sizeof(persona));

    printf("Agregue un apellido: ");
    fflush (stdin);
    gets(cliente.apellido);

    printf("Agregue un nombre: ");
    fflush (stdin);
    gets(cliente.nombres);

    do {
        printf("Agregue un tipo cliente valido (1, 2, 3) ");
        fflush (stdin);
        scanf("%d", &cliente.tipoCliente);
    } while(cliente.tipoCliente > 3 || cliente.tipoCliente <= 0);

    do {
        printf("Agregue un medio pago valido (1, 2, 3): ");
        fflush (stdin);
        scanf("%d", &cliente.medioPago);
    } while(cliente.medioPago > 3 || cliente.medioPago <= 0);

    printf("Agregue cantidad de articulos: ");
    fflush (stdin);
    scanf("%d", &cliente.cantArticulos);

    cliente.id = lastid++;
    cliente.eliminado = 0;

    return cliente;
}

persona crear_persona_condicionada(int condicion)
{
    persona cliente;
    int lastid = get_last_id(PERSONAS_PATH, sizeof(persona));

    printf("Agregue un apellido: ");
    fflush (stdin);
    gets(cliente.apellido);

    printf("Agregue un nombre: ");
    fflush (stdin);
    gets(cliente.nombres);

    do {
        printf("Agregue un tipo cliente valido (1, 2, 3) ");
        fflush (stdin);
        scanf("%d", &cliente.tipoCliente);
    } while(cliente.tipoCliente > 3 || cliente.tipoCliente <= 0);

    cliente.medioPago = condicion;

    printf("Agregue cantidad de articulos: ");
    fflush (stdin);
    scanf("%d", &cliente.cantArticulos);

    cliente.id = lastid++;
    cliente.eliminado = 0;

    return cliente;
}


/***************************************************************************
*   @brief  modifica una persona buscado por id/ cliente
*   @param  id a buscar
*   @return (void)
***************************************************************************/
void modificar_persona ()
{
        int id;
        printf("Ingrese el [ID] que desea buscar: \n");
        fflush(stdin);
        scanf("%d",&id);
        int found=0;
        char op='s';
        if(is_file_created(PERSONAS_PATH))
    {
        FILE * ptrbuffer = fopen(PERSONAS_PATH, "r+b");
        persona cliente_aux;

        while(fread(&cliente_aux, sizeof(persona), 1, ptrbuffer) > 0 && found == 0)
        {
            if(id == cliente_aux.id)
            {
                cliente_aux=crear_persona();

                found = 1;
            }

        }
        if (found==0)
        {
            printf("El [ID] buscado no se encuentra\n");
            printf("Desea buscar otro [ID]?\n");
            fflush(stdin);
            scanf("%c",&op);
            //system("PAUSE");
            if(op=='s')
            {
                modificar_persona(id);
            }
        }

        fclose(ptrbuffer);
    }
}

/***************************************************************************
*   @brief  da de baja una persona / cliente
*   @param  'nombre' nombre del usuario a darse de baja
*   @return (void)
***************************************************************************/
void baja_persona(int id)
{
    int found = 0;

    if(is_file_created(PERSONAS_PATH))
    {
        FILE * ptrbuffer = fopen(PERSONAS_PATH, "r+b");
        persona cliente_aux;

        while(fread(&cliente_aux, sizeof(persona), 1, ptrbuffer) > 0 && found == 0)
        {
            if(id == cliente_aux.id)
            {
                fseek(ptrbuffer, -1 * sizeof(persona), SEEK_CUR);

                cliente_aux.eliminado = 1;

                fwrite(&cliente_aux, sizeof(persona), 1, ptrbuffer);

                found = 1;
            }
        }

        fclose(ptrbuffer);
    }
}

/***************************************************************************
*   @brief  muestra un listado de los clientes existentes en el archivo
*   @param  (null)
*   @return (void)
***************************************************************************/
void listar_personas() {

    if(is_file_created(PERSONAS_PATH))
    {
        FILE * ptrbuffer = fopen(PERSONAS_PATH, "rb");

        persona cliente_aux;

        if(archivo_vacio(PERSONAS_PATH)) {
            printf("[ERROR] No hay clientes registrados\n");
        } else {
            while(fread(&cliente_aux, sizeof(persona), 1, ptrbuffer) > 0)
            {
                if(cliente_aux.eliminado == 1) {
                    printf("\n[PERSONA NRO %d] ", cliente_aux.id);
                    printf("// DADO DE BAJA //\n");
                } else {
                    printf("\n[PERSONA NRO %d]\n", cliente_aux.id);
                    printf("Nombre: %s %s\n", cliente_aux.nombres, cliente_aux.apellido);
                    printf("Medio de pago %d\n", cliente_aux.medioPago);
                    printf("Cantidad de articulos: %d\n", cliente_aux.cantArticulos);
                    printf("Tipo de cliente: %d\n", cliente_aux.tipoCliente);
                }
            }
        }

        system("PAUSE");
        fclose(ptrbuffer);

    }
}
