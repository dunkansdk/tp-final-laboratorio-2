#include "usuarios.h"

/***************************************************************************
*   @brief  valida la longitud y los caracteres del password ingresado
*   @param  'password'
*   @return (int) resultado de la validacion
***************************************************************************/
int validate_password(char password[])
{
    int i = 0;

    // Excede la longitud
    if(strlen(password) > 15) return 0;

    while(isalpha((int) password[i])) i++;

    // Tiene caracteres no alfabeticos
    if(i < strlen(password)) return 0;

    return 1;
}

/***************************************************************************
*   @brief  verifica si el usuario existe dentro del archivo USUARIOS_PATH
*   @param  'user' nombre del usuario
*   @return estructura del usuario en el caso de que se haya encontrado
***************************************************************************/
usuario validate_user(char user[30])
{
    FILE * ptrbuffer = fopen( USUARIOS_PATH, "rb" );

    usuario user_aux;

    while(fread(&user_aux, sizeof(usuario), 1, ptrbuffer) > 0)
    {
        if(strcmpi(user_aux.usuario, user) == 0)
        {
            fclose(ptrbuffer);
            return user_aux;
        }
    }

    fclose(ptrbuffer);
}

/***************************************************************************
*   @brief  valida si el login del usuario es correcto
*   @param  'user' usuario a evaluar (ingresado por teclado)
*   @return resultado de la validacion
***************************************************************************/
int validate_login(char user[30])
{
    float matrix_text[3][5];
    float crypted_matrix[3][5];

    float password[3][5];

    usuario user_aux = validate_user(user);

    if(user_aux.usuario) {

        while(!validate_password(password))
        {
            printf("Ingrese su password: ");
            fflush(stdin);
            gets(password);
        }

        encrypt_text(password, matrix_text);

        matrix_product(user_aux.key, matrix_text, crypted_matrix);

        if(compare_matrix(crypted_matrix, user_aux.password) == 0)
        {
            if(user_aux.eliminado) {
                system("cls");
                printf("[ERROR] El usuario se encuentra dado de baja\n");
                return 0;
            } else {
                return 1;
            }
        } else {
            system("cls");
            printf("[PASSWORD INCORRECTO] Intente nuevamente.\n");
            return 0;
        }
    }
}

/***************************************************************************
*   @brief  carga una estructura de usuario con datos ingresados por teclado
*           y se encarga de realizar la encriptacion del password
*   @param  (null)
*   @return usuario
***************************************************************************/
usuario crear_usuario()
{
    usuario user;

    float crypted_matrix[3][5]; /* Se guarda el texto encriptado */
    float password_key[3][3];   /* Llave utilizada para encriptar */
    float final_matrix[3][5];   /* Password encriptado ( KEY + MESSAGE ) */
    char password[15];

    printf("[CREAR UN USUARIO]\n");
    printf("Ingrese su nombre: ");
    fflush(stdin);
    gets(user.nombre);
    printf("Ingrese su apellido: ");
    fflush(stdin);
    gets(user.apellido);
    printf("Ingrese su nombre de usuario: ");
    fflush(stdin);
    gets(user.usuario);

    printf("Ingrese su password: ");
    fflush(stdin);
    gets(password);

    while(!validate_password(password))
    {
        printf("[ERROR] Password invalida, ingrese otra: ");
        fflush(stdin);
        gets(password);
    }

    int i, j;

    encrypt_text(password, crypted_matrix);
    generate_key(password_key);

    for(i = 0; i < 3; i++)
    {
        for(j = 0; j < 5; j++)
        {
            user.key[i][j] = password_key[i][j];
        }
    }

    matrix_product(password_key, crypted_matrix, final_matrix); /* KEY * MENSAJE = MATRIZ ENCRIPTADA */

    for(i = 0; i < 3; i++)
    {
        for(j = 0; j < 5; j++)
        {
            user.password[i][j] = final_matrix[i][j];
        }
    }

    int lastid = get_last_id(USUARIOS_PATH, sizeof(usuario));

    user.eliminado = 0;
    user.id = lastid++;

    return user;
}

/***************************************************************************
*   @brief  muestra los datos de un usuario registrado en el sistema
*   @param  'username'  nombre del usuario
*   @return (void)
***************************************************************************/
void mostrar_usuario(char username[30])
{
    FILE * ptrbuffer = fopen( USUARIOS_PATH, "rb" );
    usuario user_aux;

    float matrix_aux[3][5];
    float matrix_key[3][3];

    while(fread(&user_aux, sizeof(usuario), 1, ptrbuffer) > 0)
    {
        if(strcmpi(username, user_aux.usuario) == 0)
        {
            printf("Usuario: ");
            puts(user_aux.usuario);
            printf("Nombre: ");
            puts(user_aux.nombre);

            printf("------------- [PASSWORD CRYPTED]: \n");

            int a, b;
            for(a = 0; a < 3; a++)
            {
                for(b = 0; b < 5; b++)
                {
                    printf("\t\t%.2f ", user_aux.password[a][b]);
                }   printf("\n");
            }

            copy_matrix(matrix_key, user_aux.key);
            get_inverse(matrix_key);
            matrix_product(matrix_key, user_aux.password, matrix_aux);

            printf("------------- [PASSWORD DECRYPTED]: \n");
            for(a = 0; a < 3; a++)
            {
                for(b = 0; b < 5; b++)
                {
                    printf("\t\t%.2f ", matrix_aux[a][b]);
                }   printf("\n");
            }
        }
    }
    fclose(ptrbuffer);
}

/***************************************************************************
*   @brief  da de baja un usuario
*   @param  'username' nombre del usuario a darse de baja
*   @return (void)
***************************************************************************/
void baja_usuario(char username[30])
{
    int found = 0;

    if(is_file_created(USUARIOS_PATH))
    {
        FILE * ptrbuffer = fopen(USUARIOS_PATH, "r+b");
        usuario user_aux;

        while(fread(&user_aux, sizeof(usuario), 1, ptrbuffer) > 0 && found == 0)
        {
            if(strcmpi(username, user_aux.usuario) == 0)
            {
                fseek(ptrbuffer, -1 * sizeof(usuario), SEEK_CUR);

                user_aux.eliminado = 1;

                fwrite(&user_aux, sizeof(usuario), 1, ptrbuffer);

                found = 1;
            }
        }
        fclose(ptrbuffer);
    }
}
