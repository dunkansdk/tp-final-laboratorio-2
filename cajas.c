#include "cajas.h"

/***************************************************************************
*   @brief  funcion para inciar las 4 cajas (HARDCODE)
*   @param  'cajas' arreglo de cajas
*   @return (void)
***************************************************************************/
void iniciar_cajas(control_caja cajas[4])
{
    iniciar_cola(&cajas[0].cola);
    strcpy(cajas[0].algoritmo, "FIFO");
    strcpy(cajas[0].nombre_cajero, "CAJERO NUMERO 1");
    cajas[0].estado = 1;
    cajas[0].nro_de_caja = 1;
    cajas[0].tipo_pago = PAGO_EFECTIVO;

    iniciar_cola(&cajas[1].cola);
    strcpy(cajas[1].algoritmo, "SJF");
    strcpy(cajas[1].nombre_cajero, "CAJERO NUMERO 2");
    cajas[1].estado = 1;
    cajas[1].nro_de_caja = 2;
    cajas[1].tipo_pago = PAGO_CREDITO;

    iniciar_cola(&cajas[2].cola);
    strcpy(cajas[2].algoritmo, "Prioridad");
    strcpy(cajas[2].nombre_cajero, "CAJERO NUMERO 3");
    cajas[2].estado = 1;
    cajas[2].nro_de_caja = 3;
    cajas[2].tipo_pago = PAGO_TODOS;

    iniciar_cola(&cajas[3].cola);
    strcpy(cajas[3].algoritmo, "RoundRobin");
    strcpy(cajas[3].nombre_cajero, "CAJERO NUMERO 3");
    cajas[3].estado = 1;
    cajas[3].nro_de_caja = 4;
    cajas[3].tipo_pago = PAGO_TODOS;
}

/***************************************************************************
*   @brief  funcion para atender a los clientes existentes en las cajas
*   @param  'cajas' arreglo de cajas
*   @return (void)
***************************************************************************/
void atender_clientes(control_caja cajas[])
{
    int i;

    for(i = 0; i < CANTIDAD_CAJAS; ++i)
    {
        nodo_cola cola = cajas[i].cola;
        int c_clientes = cantidad_clientes(cola.cabecera);

        int j;

        for(j = 0; j < c_clientes; j++)
        {
            printf("Se atendio a: %s\n", cola.cabecera->cliente.nombres);
            extraer_cola(&cola);
        }

        cajas[i].cola = cola;

    }
}

/***************************************************************************
*   @brief  muestra las cajas abiertas en el arreglo
*   @param  'cajas' arreglo de cajas
*   @return (void)
***************************************************************************/
void mostrar_cajas_abiertas(control_caja cajas[4])
{
    int i;

    for(i = 0; i < 4; ++i)
    {
        if(cajas[i].estado == 1)
        {
            printf("***********************************************************\n");
            printf("*\t[CAJA NRO %d]\n", cajas[i].nro_de_caja, cajas[i].estado);
            printf("*\tALGORITMO: "); puts(cajas[i].algoritmo);
            printf("*\tCAJERO: "); puts(cajas[i].nombre_cajero);
            printf("***********************************************************\n\n");
        }
    }
}

/***************************************************************************
*   @brief  muestra una caja en particular
*   @param  'cajas' estructura de caja a la que se desea acceder
*   @return (void)
***************************************************************************/
void mostrar_caja(control_caja cajas)
{
    if(cajas.estado == 1)
    {
        printf("***********************************************************\n");
        printf("*\t[CAJA NRO %d]\n", cajas.nro_de_caja, cajas.estado);
        printf("*\tALGORITMO: "); puts(cajas.algoritmo);
        printf("*\tCAJERO: "); puts(cajas.nombre_cajero);
        printf("***********************************************************\n\n");
    } else {
        printf("[ERROR] LA CAJA SE ENCUENTRA CERRADA!\n");
    }
}

/***************************************************************************
*   @brief  cierra una determinada caja
*   @param  'cajas' estructura de caja a la que se desea acceder
*   @return (void)
***************************************************************************/
void cambiar_estado_caja(control_caja * cajas)
{
    if(cajas->cola.cabecera == NULL) {
        if(cajas->estado == 1) {
        cajas->estado = 0;
        } else {
            cajas->estado = 1;
        }
    } else {
        printf("[ERROR] La caja tiene clientes sin atender aun\n");
        system("PAUSE");
    }
}

/***************************************************************************
*   @brief  muestra un arbol preorder (TEST)
*   @param  'arbol'
*   @return (void)
***************************************************************************/
void agregar_preorder(nodo_arbol * arbol, control_caja caja[4])
{
    if(arbol != NULL)
    {
        if(arbol->cliente.medioPago == PAGO_EFECTIVO)
        {
            if(caja[PAGO_EFECTIVO - 1].estado == 1) agregar_cola(&caja[PAGO_EFECTIVO - 1].cola, arbol->cliente);
        }

        if(arbol->cliente.medioPago == PAGO_CREDITO)
        {
            if(caja[PAGO_CREDITO - 1].estado == 1) agregar_cola(&caja[PAGO_CREDITO - 1].cola, arbol->cliente);
        }

        if(arbol->cliente.medioPago == PAGO_TODOS)
        {
            if(cantidad_clientes(caja[PAGO_TODOS - 1].cola.cabecera) < cantidad_clientes(caja[PAGO_TODOS].cola.cabecera))
            {
                if(caja[PAGO_TODOS - 1].estado == 1) agregar_cola(&caja[PAGO_TODOS - 1].cola, arbol->cliente);
            } else {
                if(caja[PAGO_TODOS].estado == 1) agregar_cola(&caja[PAGO_TODOS].cola, arbol->cliente);
            }
        }

        agregar_preorder(arbol->izquierdo, caja);
        agregar_preorder(arbol->derecho, caja);
    }
}

/***************************************************************************
*   @brief  muestra un arbol inorder (TEST)
*   @param  'arbol'
*   @return (void)
***************************************************************************/
void agregar_inorder(nodo_arbol * arbol, control_caja caja[4])
{
    if(arbol != NULL)
    {
        agregar_inorder(arbol->izquierdo, caja);

        if(arbol->cliente.medioPago == PAGO_EFECTIVO)
        {
            if(caja[PAGO_EFECTIVO - 1].estado == 1) agregar_cola(&caja[PAGO_EFECTIVO - 1].cola, arbol->cliente);
        }

        if(arbol->cliente.medioPago == PAGO_CREDITO)
        {
            if(caja[PAGO_CREDITO - 1].estado == 1) agregar_cola(&caja[PAGO_CREDITO - 1].cola, arbol->cliente);
        }

        if(arbol->cliente.medioPago == PAGO_TODOS)
        {
            if(cantidad_clientes(caja[PAGO_TODOS - 1].cola.cabecera) < cantidad_clientes(caja[PAGO_TODOS].cola.cabecera))
            {
                if(caja[PAGO_TODOS - 1].estado == 1) agregar_cola(&caja[PAGO_TODOS - 1].cola, arbol->cliente);
            } else {
                if(caja[PAGO_TODOS].estado == 1) agregar_cola(&caja[PAGO_TODOS].cola, arbol->cliente);
            }
        }

        agregar_inorder(arbol->derecho, caja);
    }
}

/***************************************************************************
*   @brief  muestra un arbol posorder (TEST)
*   @param  'arbol'
*   @return (void)
***************************************************************************/
void agregar_posorder(nodo_arbol * arbol, control_caja caja[4])
{
    if(arbol != NULL)
    {
        agregar_posorder(arbol->izquierdo, caja);
        agregar_posorder(arbol->derecho, caja);

        if(arbol->cliente.medioPago == PAGO_EFECTIVO)
        {
            if(caja[PAGO_EFECTIVO - 1].estado == 1) agregar_cola(&caja[PAGO_EFECTIVO - 1].cola, arbol->cliente);
        }

        if(arbol->cliente.medioPago == PAGO_CREDITO)
        {
            if(caja[PAGO_CREDITO - 1].estado == 1) agregar_cola(&caja[PAGO_CREDITO - 1].cola, arbol->cliente);
        }

        if(arbol->cliente.medioPago == PAGO_TODOS)
        {
            if(cantidad_clientes(caja[PAGO_TODOS - 1].cola.cabecera) < cantidad_clientes(caja[PAGO_TODOS].cola.cabecera))
            {
                if(caja[PAGO_TODOS - 1].estado == 1) agregar_cola(&caja[PAGO_TODOS - 1].cola, arbol->cliente);
            } else {
                if(caja[PAGO_TODOS].estado == 1) agregar_cola(&caja[PAGO_TODOS].cola, arbol->cliente);
            }
        }
    }
}

/***************************************************************************
*   @brief  Agrega una persona al final de una cola
*   @param  'caja' caja en la que se agrega la persona
*   @return (void)
***************************************************************************/
void agregar_nueva_persona(control_caja caja)
{
    if(caja.estado == 1) {
        agregar_cola(&caja.cola, crear_persona_condicionada(caja.tipo_pago));
    } else {
        printf("[ERROR] Caja cerrada!\n");
        system("PAUSE");
    }
}

/***************************************************************************
*   @brief  Retorna el cliente nuevo agregado al final de la cola, y le asigna un tiempo determinado
*   @param  cola
*   @return retorna la cola con la persona agregada
***************************************************************************/

nodo_cola agregar_por_tiempo_Determinado (control_caja caja)
{
    persona aux = crear_persona();
    nodo_cola c;
    agregar_cola(&c,aux);

    printf("En que tiempo quiere que ingrese el cliente?\n");
    fflush(stdin);
    scanf("%d",aux.tiempoProcesado);

    return c;
}

/***************************************************************************
*   @brief  Llama la funcion agregar por tiempo determinado y la funcion obtener tiempo prioridad
*   @param  cola
*   @return
***************************************************************************/


void  resultado_por_tiempo_determinado (control_caja caja)
{
    nodo_cola cola;
    cola=agregar_por_tiempo_Determinado(caja);
    obtener_tiempos_prioridad(cola);
}
