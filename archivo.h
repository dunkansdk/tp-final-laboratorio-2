#ifndef ARCHIVO_H_INCLUDED
#define ARCHIVO_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>

#include "personas.h"
#include "listas.h"
#include "usuarios.h"

void init_file_manager();
int is_file_created(char * directory);
int get_last_id(char * directory, size_t length);
int archivo_vacio(char * dir);

void guardar_usuario(usuario user);
void guardar_cliente(persona cliente);

#endif // ARCHIVO_H_INCLUDED
