#include "arboles.h"
#include <string.h>

/***************************************************************************
*   @brief  inicializa el arbol (constructor)
*   @param  (null)
*   @return (null)
***************************************************************************/
nodo_arbol * iniciar_arbol()
{
    return NULL;
}

/***************************************************************************
*   @brief  crea un nodo con los datos de una persona
*   @param  'cliente' estructura cargada de una persona
*   @return (nodo_arbol) arbol
***************************************************************************/
nodo_arbol * crear_nodo(persona cliente)
{
    nodo_arbol * arbol = (nodo_arbol *)malloc(sizeof(nodo_arbol));

    strcpy(arbol->cliente.apellido, cliente.apellido);
    strcpy(arbol->cliente.nombres, cliente.nombres);

    arbol->cliente.cantArticulos = cliente.cantArticulos;
    arbol->cliente.eliminado = cliente.eliminado;
    arbol->cliente.id = cliente.id;
    arbol->cliente.medioPago = cliente.medioPago;
    arbol->cliente.tipoCliente = cliente.tipoCliente;

    arbol->cliente.tiempoEspera = 0;
    arbol->cliente.tiempoProcesado = 0;

    arbol->izquierdo = NULL;
    arbol->derecho = NULL;
    return arbol;
}

/***************************************************************************
*   @brief  inserta un nodo al arbol ordenandolo por nombre
*   @param  'arbol'         arbol original
*   @param  'nuevo_nodo'    nuevo nodo a ingresar
*   @return (nodo_arbol)    arbol
***************************************************************************/
nodo_arbol * insertar_nodo(nodo_arbol * arbol, nodo_arbol * nuevo_nodo)
{
    if(arbol == NULL)
        arbol = crear_nodo(nuevo_nodo->cliente);
    else
    {
        if(strcmpi(nuevo_nodo->cliente.nombres, arbol->cliente.nombres) > 0)
            arbol->derecho = insertar_nodo(arbol->derecho, nuevo_nodo);
        else
            arbol->izquierdo = insertar_nodo(arbol->izquierdo, nuevo_nodo);
    }
    return arbol;
}

/***************************************************************************
*   @brief  transfiere las estructuras de un archivo a un arbol binario
*   @param  'dir'           directorio del archivo
*   @param  'length'        tamanio de la estructura
*   @return (nodo_arbol)    arbol
***************************************************************************/
nodo_arbol * file_to_tree(char * dir, size_t length)
{
    if(is_file_created(dir))
    {
        FILE * ptrbuffer = fopen(dir, "rb");

        nodo_arbol * arbol = iniciar_arbol();
        persona cliente_aux;

        while(fread(&cliente_aux, length, 1, ptrbuffer) > 0)
        {
            arbol = insertar_nodo(arbol, crear_nodo(cliente_aux));
        }

        fclose(ptrbuffer);

        return arbol;
    }

    return NULL;
}
