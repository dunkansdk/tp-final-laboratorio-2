#include "archivo.h"

/***************************************************************************
*   @brief  verifica si un archivo esta creado
*   @param  'directory' directorio del archivo
*   @return resultado de la validacion
***************************************************************************/
int is_file_created(char * directory)
{
    FILE * ptrbuffer;

    ptrbuffer = fopen( directory, "rb" );

    if(ptrbuffer == NULL)
    {
        printf("Error al abrir archivo: ");
        puts(directory);
        fclose(ptrbuffer);
        return 0;
    }

    fclose(ptrbuffer);
    return 1;
}

/***************************************************************************
*   @brief  verfica en un archivo cual fue el ultimo id en registrarse
*   @param  'directory'     directorio del archivo
*   @param  'length'        tamanio de la estructura que desea encontrar
*   @return (int)           id resultante de la busqueda
***************************************************************************/
int get_last_id(char * directory, size_t length)
{
    int last_id = 0;

    FILE * ptrbuffer;
    ptrbuffer = fopen(directory, "rb");

    fseek(ptrbuffer, 0, SEEK_END);
    last_id = ftell(ptrbuffer) / length;
    fclose(ptrbuffer);

    return last_id;
}

/***************************************************************************
*   @brief  retorna 1 en el caso de que el archivo este vacio (pero creado)
*   @param  'dir' directorio del archivo
*   @return (boolean) true or false
***************************************************************************/
int archivo_vacio(char * dir)
{
    if(is_file_created(dir)) {

        FILE * ptrbuffer = fopen(dir, "rb");
        fseek(ptrbuffer, 0, SEEK_END);

        if(ftell(ptrbuffer) == 0) return 1;
    }

    return 0;
}

/***************************************************************************
*   @brief  inicia los archivos para poder trabajar con ellos y que no se
*           presenten problemas durante la ejecucion del programa
*   @param  (null)
*   @return (void)
***************************************************************************/
void init_file_manager()
{
    FILE * ptrbuffer;

    // En el caso de que no exista
    if(is_file_created( PERSONAS_PATH ) == NULL)
    {
        ptrbuffer = fopen( PERSONAS_PATH, "wb" ); // Creamos el archivo
        fclose(ptrbuffer);
        printf("[ARCHIVOS] Creado 'personas.bin'\n");
    }

    // Usuarios
    if(is_file_created( USUARIOS_PATH ) == NULL)
    {
        ptrbuffer = fopen( USUARIOS_PATH, "wb" ); // Creamos el archivo
        fclose(ptrbuffer);
        printf("[ARCHIVOS] Creado 'usuarios.bin'\n");
    }

}

/***************************************************************************
*   @brief  guarda una estructura usuario dentro de su respectivo archivo
*   @param  'user'  estructura del usuario
*   @return (void)
***************************************************************************/
void guardar_usuario(usuario user)
{
    FILE * ptrbuffer;

    if(is_file_created( USUARIOS_PATH ) != NULL)
    {
        ptrbuffer = fopen( USUARIOS_PATH, "ab" );

        fwrite(&user, sizeof(usuario), 1, ptrbuffer);

        fclose(ptrbuffer);

    }
}

/***************************************************************************
*   @brief  guarda una estructura persona dentro de su respectivo archivo
*   @param  'cliente'   estructura de la persona
*   @return (void)
***************************************************************************/
void guardar_cliente(persona cliente)
{
    FILE *ptrbuffer= fopen(PERSONAS_PATH, "ab");

    if(is_file_created(PERSONAS_PATH)!= NULL)
    {
        fwrite(&cliente, sizeof(persona), 1, ptrbuffer);
    }
    fclose(ptrbuffer);
}
