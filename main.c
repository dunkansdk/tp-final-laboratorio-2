#include <stdio.h>
#include <stdlib.h>

#include "menu.h"

                    /// Plantilla para comentarios ///
/***************************************************************************
*   @brief
*   @param
*   @return
***************************************************************************/

int main()
{
    init_file_manager();
    main_menu();

    return 0;
}
