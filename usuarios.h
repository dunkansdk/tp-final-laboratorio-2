#ifndef USUARIOS_H_INCLUDED
#define USUARIOS_H_INCLUDED

#include <stdio.h>
#include "matrix.h"

#define USUARIOS_PATH "usuarios.bin"

typedef struct {
    int     id;
    char    nombre[30];
    char    apellido[30];
    char    usuario[20];
    float   password[3][5];
    float   key[3][3];
    int     eliminado;
} usuario;

void mostrar_usuarios();
void mostrar_usuario(char usuario[30]);

usuario crear_usuario();
usuario validate_user(char user[30]);

int validate_password(char password[]);
int validate_login(char user[30]);

void baja_usuario(char username[30]);

#endif // USUARIOS_H_INCLUDED
