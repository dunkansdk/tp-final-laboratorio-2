#include "menu.h"
#include "arboles.h" // Provisorio

/***************************************************************************
*   @brief  menu principal del programa
*   @param  (null)
*   @return (void)
***************************************************************************/
void main_menu()
{
    int option = 0;

    while(1) {

        system("cls");

        printf("** [MENU PRINCIPAL] **\n");
        printf("1 - Ingresar al sistema\n");
        printf("2 - Crear un nuevo usuario\n");

        scanf("%d", &option);


        switch(option)
        {
        case 1:
            system("cls");
            int nuevo_usuario = 0;
            while(!login_menu(nuevo_usuario) && nuevo_usuario == 0){
                printf("\nDesea crear un nuevo usuario? (1-SI / 0-NO) . . . ");
                fflush(stdin);
                scanf("%d", &nuevo_usuario);
                system("cls");
            }
            break;

        case 2:
            system("cls");
            guardar_usuario(crear_usuario());
            break;

        default:
            system("cls");
            printf("[ERROR] Opcion incorrecta.\n");
            break;
        }
    }
}

/***************************************************************************
*   @brief  menu del log-in
*   @param  (null)
*   @return (void)
***************************************************************************/
int login_menu(int nuevo_usuario)
{
    char user[30];
    char password[30];

    if(nuevo_usuario) {
        system("cls");
        guardar_usuario(crear_usuario());
        system("cls");
    }

    printf("Ingrese su usuario: ");
    fflush(stdin);
    gets(user);

    if(validate_login(user) == 1)
    {
        usuario user_aux = validate_user(user);
        lobby_menu(user_aux);
        return 1;
    }

    return 0;
}

/***************************************************************************
*   @brief  menu del lobby (despues de loguear, control de cajas)
*   @param  (null)
*   @return (void)
***************************************************************************/
void lobby_menu(usuario user)
{
    int _error = 0;
    int option = 0;

    while(!_error) {

        system("cls");

        printf("[USUARIO ACTUAL] "); puts(user.nombre);
        printf("1 - Darse de baja\n");
        printf("2 - ABML (alta/baja/modificacion/listado) de clientes\n");
        printf("3 - Menu de cajas\n");
        printf("0 - Salir al menu principal\n");

        fflush(stdin);
        scanf("%d", &option);

        switch(option)
        {
        case 1:
            system("cls");
            baja_usuario(user.usuario);
            main_menu();
            break;

        case 2:
            abml_menu(user);
            break;

        case 3:
            seleccion_caja_menu(user);
            break;

        case 0:
            _error = 1;
            break;

        default:
            system("cls");
            printf("[ERROR] Opcion incorrecta.\n");
            break;
        }
    }

    main_menu();
}

/***************************************************************************
*   @brief  menu de la alta/baja/modificacion/listado de personas
*   @param  (null)
*   @return (void)
***************************************************************************/
void abml_menu(usuario user)
{
    int _error = 0;
    int id;

    while(!_error) {

        int option = 0;

        system("cls");
        printf("1 - Listar los clientes registrados.\n");
        printf("2 - Dar de baja una persona.\n");
        printf("3 - Dar de alta una persona.\n");
        printf("4 - Modificar una persona.\n");
        printf("0 - Volver atras.\n");
        printf("Que opcion desea elegir?: ");

        fflush(stdin);
        scanf("%d", &option);

        switch(option)
        {
        case 1:
            listar_personas();
            break;

        case 2:
            system("cls");
            printf("Que persona quiere dar de baja (Ingresar ID)?: ");
            scanf("%d", &id);
            baja_persona(id);
            break;

        case 3:
            system("cls");
            guardar_cliente(crear_persona());
            break;

        case 4:
            system("cls");
            fflush(stdin);
            /*printf("Que persona quiere modificar (Ingresar ID)?: ");
            scanf("%d", &id);*/
            modificar_persona();
            break;

        case 0:
            _error = 1;
            break;

        default:
            system("cls");
            printf("[ERROR] Opcion incorrecta.\n");
            system("PAUSE");
            break;
        }
    }

    lobby_menu(user);
}

/***************************************************************************
*   @brief  Menu para seleccionar la caja en la que se desea trabjar
*   @param  'user' usuario logueado en el sistema
*   @return (void)
***************************************************************************/
void seleccion_caja_menu(usuario user)
{
    int _error = 0;
    int option = 0;

    // Creamos el arreglo principal de cajas
    control_caja cajas[4];
    iniciar_cajas(cajas);

    // Creamos el arbol y agregamos los clientes del archivo
    nodo_arbol * arbol = iniciar_arbol();
    arbol = file_to_tree(PERSONAS_PATH, sizeof(persona));

    while(!_error) {

        system("cls");

        printf("[MENU PRINCIPAL DE CAJAS]\n");
        printf("1 - Cargar las cajas con los datos en el arbol\n");
        printf("2 - Obtener el tiempo de ejecucion en una caja\n");
        printf("3 - Agregar un cliente a una caja\n");
        printf("4 - Agregar un cliente en tiempo determinado\n");
        printf("5 - Atender a los clientes\n");
        printf("6 - Cerrar una caja\n");

        printf("0 - Volver atras\n");

        fflush(stdin);
        scanf("%d", &option);

        switch(option)
        {
        case 1:
            cargar_colas_menu(planificacion_option(), arbol, cajas);
            break;

        case 2:
            mostrar_tiempo_menu(user, cajas[caja_actual_option()]);
            break;

        case 3:
            agregar_nueva_persona(cajas[caja_actual_option()]);
            break;

        case 4:
            agregar_por_tiempo_Determinado(cajas[caja_actual_option()]);
            system("PAUSE");
            break;

        case 5:
            atender_clientes(cajas);
            system("PAUSE");
            break;

        case 6:
            cambiar_estado_caja(&cajas[caja_actual_option()]);
            break;

        case 0:
            _error = 1;
            break;

        default:
            system("cls");
            printf("[ERROR] Opcion incorrecta.\n");
            break;
        }
    }

    lobby_menu(user);
}

/***************************************************************************
*   @brief  Menu principal de las cajas, se elige el tipo de ordenamiento
*   @param  'user' usuario logueado en el sistema
*   @return (void)
***************************************************************************/
order_type planificacion_option()
{
    int _error = 0;
    int option = 0;

    while(!_error) {

        system("cls");
        printf("1 - Ordenar por preorder\n");
        printf("2 - Ordenar por inorder\n");
        printf("3 - Ordenar por posorder\n");

        fflush(stdin);
        scanf("%d", &option);

        switch(option)
        {
        case 1: case 2: case 3:
            return option;
            break;

        case 0:
            _error = 1;
            break;

        default:
            _error = 1;
            system("cls");
            printf("[ERROR] Opcion incorrecta.\n");
            break;
        }
    }
}

/***************************************************************************
*   @brief  Menu que administra el numero de la caja en la que se desea trabajar
*   @param  (null)
*   @return (int) caja seleccionada
***************************************************************************/
int caja_actual_option()
{
    int _error = 0;
    int option = 0;

    while(!_error) {

        system("cls");
        printf("1 - Caja n1 (FIFO)\n");
        printf("2 - Caja n2 (SJF)\n");
        printf("3 - Caja n3 (Propiedades apropiativo)\n");
        printf("4 - Caja n4 (Round Robin Quantum 7)\n");

        fflush(stdin);
        scanf("%d", &option);

        switch(option)
        {
        case 1: case 2: case 3: case 4:
            return option - 1;
            break;

        case 0:
            _error = 1;
            break;

        default:
            system("cls");
            printf("[ERROR] Opcion incorrecta.\n");
            break;
        }
    }
}

/***************************************************************************
*   @brief  Menu interno de las cajas
*   @param  'order' enum que determina el tipo de ordenamiento
*   @param  'arbol' arbol donde se encuentran los clientes del archivo
*   @param  'cajas' arreglo de cajas
*   @return (void)
***************************************************************************/
void cargar_colas_menu(order_type order, nodo_arbol * arbol, control_caja cajas[4])
{
    switch(order)
    {
    case 1:
        agregar_preorder(arbol, cajas);
        break;

    case 2:
        agregar_inorder(arbol, cajas);
        break;

    case 3:
        agregar_posorder(arbol, cajas);
        break;

    default:
        system("cls");
        printf("[ERROR] No existe un algoritmo definido, el programa va a cerrarse.");
        system("PAUSE");
        exit(0);
        break;
    }
}

/***************************************************************************
*   @brief  Muestra el tiempo de ejecucion de la caja seleccionada
*   @param  'user' se utiliza para volver al lobby
*   @return 'caja_actual' caja seleccionada para la operacion
***************************************************************************/
void mostrar_tiempo_menu(usuario user, control_caja caja_actual)
{
    if(strcmpi(caja_actual.algoritmo, "FIFO") == 0)
    {
        if(caja_actual.estado == 1) {
            mostrar_caja(caja_actual);
            obtener_tiempos_FIFO(caja_actual.cola);
        } else {
            printf("[ERROR] Caja cerrada!\n");
        }
    }

    if(strcmpi(caja_actual.algoritmo, "SJF") == 0)
    {
        if(caja_actual.estado == 1) {
            mostrar_caja(caja_actual);
            obtener_tiempos_SJF(caja_actual.cola);
        } else {
            printf("[ERROR] Caja cerrada!\n");
        }
    }

    if(strcmpi(caja_actual.algoritmo, "Prioridad") == 0)
    {
        if(caja_actual.estado == 1) {
            mostrar_caja(caja_actual);
            obtener_tiempos_prioridad(caja_actual.cola);
        } else {
            printf("[ERROR] Caja cerrada!\n");
        }
    }

    if(strcmpi(caja_actual.algoritmo, "RoundRobin") == 0)
    {
        if(caja_actual.estado == 1) {
            mostrar_caja(caja_actual);
            obtener_tiempos_rr(caja_actual.cola);
        } else {
            printf("[ERROR] Caja cerrada!\n");
        }
    }

    system("PAUSE");
}
