#include "planificacion.h"

/***************************************************************************
*   @brief  Obtiene los tiempos de espera y respuesta de FIFO
*   @param  (cola)
*   @return (void)
***************************************************************************/
void obtener_tiempos_FIFO(nodo_cola cola)
{
    cola.cabecera = setear_tiempos(cola);
    mostrar_cola_ctiempos(cola);

    nodo_lista * aux_lista = cola.cabecera;

    float t_espera = 0.0f;
    float t_respuesta = 0.0f;

    while(aux_lista != NULL)
    {
        t_espera += (float)aux_lista->cliente.tiempoEspera;
        t_respuesta += (float)aux_lista->cliente.tiempoProcesado;
        aux_lista = aux_lista->siguiente;
    }

    printf("\n * Tiempo de espera: %.2f\n", t_espera / (float)cantidad_clientes(cola.cabecera));
    printf(" * Tiempo de respuesta: %.2f\n", t_respuesta / (float)cantidad_clientes(cola.cabecera));
}

/***************************************************************************
*   @brief  Obtiene los tiempos de espera y respuesta de SJF y ordena la cola
*   @param  (cola)
*   @return (void)
***************************************************************************/
void obtener_tiempos_SJF(nodo_cola cola)
{
    cola.cabecera = ordenar_lista(cola.cabecera); // Ordenamos la lista para cumplir con las condiciones de SJF
    cola.cabecera = setear_tiempos(cola);
    mostrar_cola_ctiempos(cola);

    nodo_lista * aux_lista = cola.cabecera;

    float t_espera = 0.0f;
    float t_respuesta = 0.0f;

    while(aux_lista != NULL)
    {
        t_espera += (float)aux_lista->cliente.tiempoEspera;
        t_respuesta += (float)aux_lista->cliente.tiempoProcesado;
        aux_lista = aux_lista->siguiente;
    }

    printf("\n * Tiempo de espera: %.2f\n", t_espera / (float)cantidad_clientes(cola.cabecera));
    printf(" * Tiempo de respuesta: %.2f\n", t_respuesta / (float)cantidad_clientes(cola.cabecera));
}

/***************************************************************************
*   @brief  Obtiene los tiempos de espera y respuesta de prioridad
*           y ordena la cola
*   @param  (cola)
*   @return (void)
***************************************************************************/
void obtener_tiempos_prioridad(nodo_cola cola)
{
    cola.cabecera = ordenar_lista_prioridad(cola.cabecera); // Ordenamos la lista para cumplir con las condiciones de SJF
    cola.cabecera = setear_tiempos(cola);
    mostrar_cola_ctiempos(cola);

    nodo_lista * aux_lista = cola.cabecera;

    float t_espera = 0.0f;
    float t_respuesta = 0.0f;

    while(aux_lista != NULL)
    {
        t_espera += (float)aux_lista->cliente.tiempoEspera;
        t_respuesta += (float)aux_lista->cliente.tiempoProcesado;
        aux_lista = aux_lista->siguiente;
    }

    printf("\n * Tiempo de espera: %.2f\n", t_espera / (float)cantidad_clientes(cola.cabecera));
    printf("* Tiempo de respuesta: %.2f\n", t_respuesta / (float)cantidad_clientes(cola.cabecera));
}

/***************************************************************************
*   @brief  Muestra los tiempos seteados en las estructuras de la cola
*   @param  (cola)
*   @return (void)
***************************************************************************/
void mostrar_cola_ctiempos(nodo_cola cola)
{
    nodo_lista * lista = cola.cabecera;

    while(lista != NULL)
    {
        printf("|\t%s\t\t|\t%d\t|\t%d\t|\t%d\t|\n", lista->cliente.nombres, lista->cliente.cantArticulos, lista->cliente.tiempoEspera, lista->cliente.tiempoProcesado);
        lista = lista->siguiente;
    }
}

/***************************************************************************
*   @brief  Setea los tiempos de espera y respuesta de los clientes
*   @param  (cola)
*   @return (nodo_lista) puntero a la cabcecera de la cola
***************************************************************************/
nodo_lista * setear_tiempos(nodo_cola cola)
{
    int t_anterior = 0;

    nodo_lista * seguidora = cola.cabecera;

    while(seguidora != NULL)
    {
        seguidora->cliente.tiempoEspera = t_anterior;
        t_anterior += seguidora->cliente.cantArticulos;
        seguidora->cliente.tiempoProcesado = t_anterior;
        seguidora = seguidora->siguiente;
    }

    return cola.cabecera;
}


/***************************************************************************
*   @brief  Carga el arreglo t_actual con las rafagas de los clientes
*   @param  'tiempo' arreglo de rafagas
*   @param  'lista' lista que guarda los clientes
*   @return (void)
***************************************************************************/
void cargar_tiempos(int tiempo[], nodo_lista * lista)
{
    int i = 0;

    while(lista != NULL)
    {
        tiempo[i] = lista->cliente.cantArticulos;
        i++;
        lista = lista->siguiente;
    }
}

/***************************************************************************
*   @brief  Retorna los tiempos de ejecucion y procesado de roundrobin
*   @param  (nodo_cola)
*   @return (void)
***************************************************************************/
void obtener_tiempos_rr(nodo_cola cola)
{
    nodo_lista * aux_lista = round_robin(cola, 7);

    float t_espera = 0.0f;
    float t_respuesta = 0.0f;

    while(aux_lista != NULL)
    {
        t_espera += (float)aux_lista->cliente.tiempoEspera;
        t_respuesta += (float)aux_lista->cliente.tiempoProcesado;
        printf("|\t%s    \t\t|\t%.2f\t|\t%.2f\t|\n", aux_lista->cliente.nombres, t_espera / (float)cantidad_clientes(cola.cabecera), t_respuesta / (float)cantidad_clientes(cola.cabecera));
        aux_lista = aux_lista->siguiente;
    }

    printf("\n * Tiempo de espera: %.2f\n", t_espera / (float)cantidad_clientes(cola.cabecera));
    printf(" * Tiempo de respuesta: %.2f\n", t_respuesta / (float)cantidad_clientes(cola.cabecera));
}

/***************************************************************************
*   @brief  obtiene los timpos de respuesta y procesado segun RR
*   @param  (nodo_cola)
*   @param  'quantum' (default 7)
*   @return puntero al principio de la lista
***************************************************************************/
nodo_lista * round_robin(nodo_cola cola, int quantum)
{
    nodo_lista * lista = cola.cabecera;

    int nro_clientes = cantidad_clientes(cola.cabecera);
    int t_actual[nro_clientes];
    cargar_tiempos(t_actual, cola.cabecera);

    int t_counter = 0;
    int t_anterior = 0;

    while(tiempos_activos_rr(t_actual, nro_clientes) == 1)
    {
        while(lista != NULL)
        {
            int q_resultado = 0;
            t_anterior += t_actual[t_counter];

            if(t_actual[t_counter] == lista->cliente.cantArticulos) lista->cliente.tiempoEspera = t_anterior;
            //printf("|\t%s      \t\t|\t%d\t|\t%.2f\t", lista->cliente.nombres, t_actual[t_counter], lista->cliente.tiempoEspera);
            t_actual[t_counter] = result_quantum(t_actual[t_counter], quantum);
            if(t_actual[t_counter] == 0 && lista->cliente.tiempoProcesado == 0) lista->cliente.tiempoProcesado = t_anterior;
            //printf("|\t%.2f\t|\n", lista->cliente.tiempoProcesado);

            if(t_counter == nro_clientes - 1)
                t_counter = 0;
            else
                t_counter++;

            lista = lista->siguiente;
        }

        lista = cola.cabecera;
    }

    return lista;
}

/***************************************************************************
*   @brief  verifica si quedan articulos por procesar entre los clientes
*   @param  'tiempos' arreglo donde se guardan las rafagas
*   @param  'dimension' cantidad de clientes en la caja
*   @return (boolean)
***************************************************************************/
int tiempos_activos_rr(int tiempos[], int dimension)
{
    int i;
    int t_counter = 0;

    for(i = 0; i < dimension; ++i)
    {
        if(tiempos[i] == 0) {
            t_counter++;
        }
    }

    if(t_counter == dimension) return 0;

    return 1;
}

/***************************************************************************
*   @brief  Obtiene el resultado de la rafaga - quantum
*   @param  cantidad_articulos
*   @param  quantum (7 default)
*   @return la cantidad de articulos pendientes
***************************************************************************/
int result_quantum(int cantidad_articulos, int quantum)
{
    int i;

    if(cantidad_articulos == 0) return 0;

    for(i = 0; i < quantum; ++i)
    {
        if(i == cantidad_articulos) return 0;
    }

    return cantidad_articulos - quantum;
}


