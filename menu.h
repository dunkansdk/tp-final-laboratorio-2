#ifndef MENU_H_INCLUDED
#define MENU_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>

#include "archivo.h"
#include "cajas.h"
#include "planificacion.h"

void main_menu();
int login_menu(int nuevo_usuario);
void lobby_menu(usuario user);
void abml_menu(usuario user);

void seleccion_caja_menu(usuario user);
void cargar_colas_menu(order_type order, nodo_arbol * arbol, control_caja cajas[4]);
void principal_caja_menu(usuario user, int action, order_type order, control_caja caja_actual);

order_type planificacion_option();
int caja_actual_option();

#endif // MENU_H_INCLUDED
