#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>

void copy_matrix(float matrix_1[3][3], float matrix_2[3][3]);
void generate_key(float matrix[3][3]);
void generate_encrypted_matrix(float crypted_str[50], int value, float crypted_matrix[3][5]);
void encrypt_text(char original_str[15], float crypted_matrix[3][5]);
float math_prod(float matrix_1[3][3], float matrix_2[3][3], int a, int b);
void matrix_product(float matrix_1[3][3], float matrix_2[3][5], float result[3][5]);
float get_determinant(float matrix[3][3]);
void get_attached(float matrix[3][3]);
void get_inverse(float matrix[3][3]);
int compare_matrix(float matrix_1[3][5], float matrix_2[3][5]);
