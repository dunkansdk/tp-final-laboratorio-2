#ifndef ARBOLES_H_INCLUDED
#define ARBOLES_H_INCLUDED

#include "archivo.h"
#include "cola.h"

typedef struct
{
    persona cliente;
    struct nodo_arbol * izquierdo;
    struct nodo_arbol * derecho;
} nodo_arbol;

nodo_arbol * iniciar_arbol();
nodo_arbol * crear_nodo(persona cliente);
nodo_arbol * insertar_nodo(nodo_arbol * arbol, nodo_arbol * nuevo_nodo);
nodo_arbol * file_to_tree(char * dir, size_t length);

#endif // ARBOLES_H_INCLUDED
