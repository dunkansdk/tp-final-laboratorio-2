#ifndef PLANIFICACION_H_INCLUDED
#define PLANIFICACION_H_INCLUDED

#include "cola.h"

void obtener_tiempos_FIFO(nodo_cola cola);
void obtener_tiempos_SJF(nodo_cola cola);
void obtener_tiempos_prioridad(nodo_cola cola);
void obtener_tiempos_rr(nodo_cola cola);

nodo_lista * setear_tiempos(nodo_cola cola);

nodo_lista * round_robin(nodo_cola cola, int quantum);

/*funciones adicionales*/
void obtener_tiempos_prioridad(nodo_cola cola);
void mostrar_cola_ctiempos(nodo_cola cola);
void cargar_tiempos(int tiempo[], nodo_lista * lista);
int tiempos_activos_rr(int tiempos[], int dimension);
int result_quantum(int cantidad_articulos, int quantum);




#endif // PLANIFICACION_H_INCLUDED
