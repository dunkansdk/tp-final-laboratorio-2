#ifndef COLA_H_INCLUDED
#define COLA_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>

#include "listas.h"

typedef struct {
    nodo_lista * cabecera;
    nodo_lista * ultimo;
}   nodo_cola;

void iniciar_cola(nodo_cola * cola);
void agregar_cola(nodo_cola * cola, persona cliente);
persona extraer_cola(nodo_cola * cola);

void mostrar_cola(nodo_cola cola);

#endif // COLA_H_INCLUDED
