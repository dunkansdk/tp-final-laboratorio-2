#ifndef PERSONAS_H_INCLUDED
#define PERSONAS_H_INCLUDED

#define PERSONAS_PATH "personas.bin"

typedef struct {
    int     id;
    char    apellido[30];
    char    nombres[40];
    int     tipoCliente;            /// Prioridad 1:embarazada, 2:jubilado y 3:cliente com�n
    int     medioPago;              /// 1:efectivo, 2:cr�dito y 3:todos
    int     cantArticulos;          /// es el tiempo de ejecuci�n
    int     tiempoEspera;           /// es el tiempo de respuesta
    int     tiempoProcesado;        /// es el tiempo que ya fue procesado en la l�nea de caja
    int     eliminado;              /// 1: eliminado, 0: activo
} persona;

typedef enum e_prioridad
{
        PRIORIDAD_EMBARAZADA    = 1,
        PRIORIDAD_JUBILADO      = 2,
        PRIORIDAD_CLIENTE       = 3
} tipo_cliente;

typedef enum e_pago
{
        PAGO_EFECTIVO   = 1,
        PAGO_CREDITO    = 2,
        PAGO_TODOS      = 3
} medio_pago;

persona crear_persona();
persona crear_persona_condicionada(int condicion);

void baja_persona(int id);
void listar_personas();
void modificar_persona ();

#endif // PERSONAS_H_INCLUDED
